import React, { useState } from 'react';
import { StatusBar } from 'react-native';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';

import Routes from './routes';

export default function App() {
  const [fontLoaded, setFontLoaded] = useState<boolean>(false);

  async function loadFonts() {
      console.log('teste')
      await Font.loadAsync({
          GilroyBold: require('./assets/fonts/Gilroy-Bold.ttf'),
          GilroyHeavy: require('./assets/fonts/Gilroy-Heavy.ttf'),
          GilroyMedium: require('./assets/fonts/Gilroy-Medium.ttf'),
          GilroySemiBold: require('./assets/fonts/Gilroy-SemiBold.ttf'),
          GilroyRegular: require('./assets/fonts/Gilroy-Regular.ttf'),
      });

      setFontLoaded(true);
  }

  loadFonts();

  if(!fontLoaded){
      return <AppLoading/>;
  }

  return (
    <>  
      <StatusBar translucent backgroundColor="#f4f4f4" barStyle="dark-content"/>
      <Routes/> 
    </>
  );
}

