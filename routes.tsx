import React from 'react';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Home from './pages/Home';
import CharDetail from './pages/CharDetail';

const Stack = createStackNavigator();

const Routes = () => {
    return(
        <NavigationContainer>
            <Stack.Navigator headerMode="none" initialRouteName="Home" screenOptions={{
                cardStyle: {
                    backgroundColor: "#f4f4f4",
                }
            }}>
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="CharDetail" component={CharDetail} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default Routes;