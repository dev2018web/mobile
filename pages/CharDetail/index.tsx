import React, { useState, useEffect } from 'react';
import { Text, View, Image, ScrollView, TouchableOpacity, Dimensions, ImageBackground, Slider } from 'react-native';
import styled from 'styled-components';
import Constants from 'expo-constants';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Box, PersonName, MovieName } from '../Home';
import Back from '../../assets/icons/back.svg';
import Age from '../../assets/icons/age.svg';
import Weight from '../../assets/icons/weight.svg';
import Height from '../../assets/icons/height.svg';
import Universe from '../../assets/icons/universe.svg';
import { AppLoading } from 'expo';

const Header = styled(View)`
    width: 100%;
    height: 80px;
    flex-direction: row;
    justify-content: space-between;
    padding: 0px 18px 0;
`;

const SliderMovie = styled(ScrollView)`
    padding-bottom: 10px;
    margin: 10px 0 10px;
    flex-direction: row;
`;

const Content = styled(View)`
    padding: 10px;
`;

const BoxPersonagens = styled(View)`
    padding-bottom: 30px;
`;

const SliderItem = styled(Image)`
    width: 150px;
    height: 250;
    border-radius: 10px;
    padding-bottom: 10;
    flex-direction: row;
    align-items: flex-end;
`;

const ItemImage = styled(Image)`
    width: 150px;
    height: 250;
    border-radius: 10;
    padding-bottom: 10;
    margin: 0 10px 0 0;
`;


const Thumb = styled(ImageBackground)`
    justify-content: space-between;   
`;

const BackIcon = styled(Back)`
    color: white;
`;

const BoxQualities = styled(View)`
    flex-direction: row;
    justify-content: space-around;
    padding: 5px 8px;
    background: rgba(0,0,0,0.4)
`;

const BoxItem = styled(View)`
    align-items: center;
`;

const Hability = styled(View)`
    flex-direction: row;
    align-items: flex-end;
`;


const BoxHabilities = styled(View)`
    padding: 0 10px 30px;
`;

const HabilitName = styled(Text)`
    color: white;
    width: 100px;
`;

const BoxMovies = styled(View)`
    width: 100%;
    padding: 0 0 40px;
`;

const dimensions = Dimensions.get('window');
const imageHeight = Math.round(dimensions.width * 9 / 16);
const imageWidth = dimensions.width;


interface Ability {
    agility: number,
    endurance: number,
    force: number,
    intelligence: number,
    velocity: number
}

interface Object {
    alterEgo: string,
    biography: string,
    imagePath: any,
    name: string,
}

interface Caracteristic {
    birth: number,
    height: {
        unity: string,
        value: number,
    },
    universe: string,
    weight: {
        unity: string,
        value: number,
    },
}

interface Params {
    abilities: {
        agility: number,
        endurance: number,
        force: number,
        intelligence: number,
        velocity: number
    },
    alterEgo: string,
    biography: string,
    imagePath: number,
    name: string,
    caracteristics: {
        birth: number,
        height: {
            unity: string,
            value: number,
        },
        universe: string,
        weight: {
            unity: string,
            value: number,
        },
    },
    movies: Array<string>
}



const CharDetail = () => {
    const route = useRoute();
    const routeParams = route.params as Params;
    const navigation = useNavigation();


    const [object, setObject] = useState<Object>();
    const [abilities, setAbilities] = useState<Ability>();
    const [caracteristics, setCaracteristics] = useState<Caracteristic>();
    const [movies, setMovies] = useState<string[]>([]);

    function navigateToHome() {
        navigation.goBack();
    }

    useEffect(() => {
        const { agility, endurance, force, intelligence, velocity } = routeParams.abilities;
        const { alterEgo, biography, imagePath, name } = routeParams;
        const { birth, height, universe, weight } = routeParams.caracteristics;

        setAbilities({ agility, endurance, force, intelligence, velocity });
        setObject({ alterEgo, biography, imagePath, name });
        setCaracteristics({ birth, height, universe, weight });
        setMovies(routeParams.movies);
    }, []);

    if (!caracteristics || !abilities) {
        return null;
    }

    const { birth, weight, height, universe } = caracteristics;
    const { agility, endurance, force, intelligence, velocity } = abilities;


    return (
        <Box style={{ paddingTop: Constants.statusBarHeight, backgroundColor: "#000000" }}>
            <Thumb source={object?.imagePath} style={{
                position: 'relative',
                height: imageHeight + 200
            }}>
                <View style={{ padding: 15 }}>
                    <Back onPress={navigateToHome} />
                </View>

                <View style={{ flexGrow: 2, justifyContent: 'flex-end', padding: 10, paddingBottom: 15 }}>
                    <PersonName  style={{ fontSize: 16, fontFamily: 'GilroyMedium' }}>{object?.alterEgo}</PersonName>
                    <MovieName style={{ fontSize: 40, fontFamily: 'GilroyHeavy' }}>{object?.name}</MovieName>
                </View>
                <BoxQualities>
                    <BoxItem>
                        <Age width={25} height={25} />
                        <Text style={{ color: 'white', fontSize: 12, fontFamily: 'GilroyMedium', paddingTop: 4 }}>{birth ?? birth}</Text>
                    </BoxItem>
                    <BoxItem>
                        <Weight width={25} height={25} />
                        <Text style={{ color: 'white', fontFamily: 'GilroyMedium', paddingTop: 4  }}>{`${weight?.value} ${weight?.unity}`}</Text>
                    </BoxItem>
                    <BoxItem>
                        <Height width={25} height={25} />
                        <Text style={{ color: 'white', fontFamily: 'GilroyMedium', paddingTop: 4  }}>{`${height?.value} ${height?.unity}`}</Text>
                    </BoxItem>
                    <BoxItem>
                        <Universe width={25} height={25} />
                        <Text style={{ color: 'white', fontFamily: 'GilroyMedium', paddingTop: 4  }}>{universe}</Text>
                    </BoxItem>
                </BoxQualities>
            </Thumb>
            <Content>
                <Text style={{ color: '#929294', fontSize: 14, fontFamily: 'GilroyMedium' }}>{object?.biography}</Text>
            </Content>
            <BoxHabilities>
                <Text style={{ color: 'white', fontSize: 18, fontFamily: "GilroyBold", paddingTop: 20}}>Habilidades</Text>

                <Hability style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                    <HabilitName style={{fontFamily: 'GilroyRegular', color: '#9b9b9b', marginTop: 12}}>Força</HabilitName>
                    <Slider
                        style={{ width: 150, height: 10 }}
                        minimumValue={0}
                        maximumValue={100}
                        value={force}
                        minimumTrackTintColor="#FFFFFF"
                        maximumTrackTintColor="red"
                    />
                </Hability>
                <Hability style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                    <HabilitName style={{fontFamily: 'GilroyRegular', color: '#9b9b9b', marginTop: 12}}>Inteligência</HabilitName>
                    <Slider
                        style={{ width: 150, height: 10 }}
                        minimumValue={0}
                        maximumValue={100}
                        value={intelligence}
                        minimumTrackTintColor="#FFFFFF"
                        maximumTrackTintColor="red"
                    />
                </Hability>
                <Hability style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                    <HabilitName style={{fontFamily: 'GilroyRegular', color: '#9b9b9b', marginTop: 12}}>Agilidade</HabilitName>
                    <Slider
                        style={{ width: 150, height: 10 }}
                        minimumValue={0}
                        maximumValue={100}
                        value={agility}
                        minimumTrackTintColor="#FFFFFF"
                        maximumTrackTintColor="red"
                    />
                </Hability>
                <Hability style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                    <HabilitName style={{fontFamily: 'GilroyRegular', color: '#9b9b9b', marginTop: 12}}>Velocidade</HabilitName>
                    <Slider
                        style={{ width: 150, height: 10 }}
                        minimumValue={0}
                        maximumValue={100}
                        value={velocity}
                        minimumTrackTintColor="#FFFFFF"
                        maximumTrackTintColor="red"
                    />
                </Hability>
            </BoxHabilities>
            <BoxMovies>
                <Text style={{ color: 'white', fontSize: 18, fontFamily: "GilroyBold", paddingBottom: 8, paddingLeft: 10}}>Filmes</Text>
                <SliderMovie
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{ paddingHorizontal: 16 }}>
                    {movies?.map((item: any) => (
                        <View key={item} style={{ marginRight: 10 }}>
                            <SliderItem key={item} source={item}/>
                        </View>
                    ))}
                </SliderMovie>
            </BoxMovies>
        </Box>
    )
}

export default CharDetail;