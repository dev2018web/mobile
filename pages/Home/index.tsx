import React, { useState, useEffect } from 'react';
import { Text, View, Image, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import styled from 'styled-components';
import MenuIcon from '../../assets/icons/menu.svg';
import TitleIcon from '../../assets/icons/marvel.svg';
import SearchIcon from '../../assets/icons/search.svg';
import HeroIcon from '../../assets/icons/hero.svg';
import HumanIcon from '../../assets/icons/human.svg';
import AlienIcon from '../../assets/icons/alien.svg';
import AntiheroIcon from '../../assets/icons/antihero.svg';
import VillainIcon from '../../assets/icons/villain.svg';
import { LinearGradient } from 'expo-linear-gradient';
import Constants from 'expo-constants';
import application from '../../assets/application';
import { useNavigation } from '@react-navigation/native';

export const Box = styled(ScrollView)`
    padding: 0px 0px 230px;
`;

const Header = styled(View)`
    width: 100%;
    height: 80px;
    flex-direction: row;
    justify-content: space-between;
    padding: 0px 18px 0;
`;

const Content = styled(View)`
    background-color: blue;
    width: 100%;
    height: 80px;
    font-family: 'gilroy-bold'
`;

const Categories = styled(View)`
    flex-direction: row;
    justify-content: space-between;
    padding: 40px 18px;
`;

const BoxPersonagens = styled(View)`
    padding-bottom: 30px;
`;

const Slider = styled(ScrollView)`
    padding-bottom: 10px;
    margin: 10px 0 10px;
    flex-direction: row;
`;
const SliderItem = styled(ImageBackground)`
    width: 130px;
    height: 210;
    border-radius: 100px;
    padding-bottom: 10;
    flex-direction: row;
    align-items: flex-end;
`;

const ItemImage = styled(Image)`
    width: 150px;
    height: 250;
    border-radius: 10px;
    padding-bottom: 10;
    margin: 0 10px 0 0;
`;

export const PersonName = styled(Text)`
    color: white;
    font-size: 10px;
    padding-left: 5px;
`;

export const MovieName = styled(Text)`
    color: white;
    font-size: 20px;
    font-weight: bold;
    padding-left: 5px;
`;

const Home = () => {
    const navigation = useNavigation();

    function handleNavigateToDetail(item: Object) {
        navigation.navigate('CharDetail', item);
    }


    return (
        <>
            <Header style={{ paddingTop: 10 + Constants.statusBarHeight }}>
                <MenuIcon />
                <TitleIcon />
                <SearchIcon />
            </Header>

            <Box style={{ paddingTop: 10 + Constants.statusBarHeight }}>
                <View style={{ paddingLeft: 18, paddingRight: 18 }}>
                    <Text style={{ fontSize: 12, fontFamily: 'GilroySemiBold', color: "#bbbbcb" }}>Bem-vindo ao Marvel Heroes</Text>
                    <Text style={{ fontSize: 40, fontFamily: 'GilroyBold', color: '#313140' }}>Escolha o seu personagem</Text>
                </View>
                <Categories>
                    <LinearGradient
                        colors={['#015cea', '#01c5fb']}
                        style={{ borderRadius: 100, padding: 10 }}>
                        <HeroIcon width="32" height="32" />
                    </LinearGradient>
                    <LinearGradient
                        colors={['#ed1d25', '#ed1f68']}
                        style={{ borderRadius: 100, padding: 10 }}>
                        <VillainIcon width="32" height="32" />
                    </LinearGradient>
                    <LinearGradient
                        colors={['#b027f0', '#7679ff']}
                        style={{ borderRadius: 100, padding: 10 }}>
                        <AntiheroIcon width="32" height="32" />
                    </LinearGradient>
                    <LinearGradient
                        colors={['#0ca361', '#3bb991']}
                        style={{ borderRadius: 100, padding: 10 }}>
                        <AlienIcon width="32" height="32" />
                    </LinearGradient>
                    <LinearGradient
                        colors={['#ff7eb3', '#ff758c']}
                        style={{ borderRadius: 100, padding: 10 }}>
                        <HumanIcon width="32" height="32" />
                    </LinearGradient>
                </Categories>
                <BoxPersonagens>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end', paddingLeft: 18, paddingRight: 18 }}>
                        <Text style={{ color: '#f2264b', fontFamily: 'GilroyBold', fontSize: 18 }}>Heróis</Text>
                        <Text style={{ color: '#b7b7c8', fontWeight: 'bold' }}>Ver tudo</Text>
                    </View>
                    <Slider
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{ paddingHorizontal: 16 }}>
                        {application?.heroes.map(item => (
                            <TouchableOpacity key={item.name} style={{ borderRadius: 10, overflow: 'hidden', marginRight: 10 }} onPress={() => handleNavigateToDetail(item)}>
                                <SliderItem key={item?.name} source={item?.imagePath}>
                                    <View>
                                        <PersonName style={{fontFamily: 'GilroyMedium'}}>{item?.alterEgo}</PersonName>
                                        <MovieName style={{fontFamily: 'GilroyHeavy'}}>{item?.name}</MovieName>
                                    </View>
                                </SliderItem>
                            </TouchableOpacity>
                        ))}
                    </Slider>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end', paddingLeft: 18, paddingRight: 18 }}>
                        <Text style={{ color: '#f2264b', fontFamily: 'GilroyBold', fontSize: 18 }}>Vilões</Text>
                        <Text style={{ color: '#b7b7c8', fontWeight: 'bold' }}>Ver tudo</Text>
                    </View>
                    <Slider
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{ paddingHorizontal: 16 }}>
                        {application?.villains.map(item => (
                            <TouchableOpacity key={item.name} style={{ borderRadius: 10, overflow: 'hidden', marginRight: 10 }} onPress={() => handleNavigateToDetail(item)}>
                                <SliderItem key={item?.name} source={item?.imagePath}>
                                    <View>
                                        <PersonName style={{fontFamily: 'GilroyMedium'}}>{item?.alterEgo}</PersonName>
                                        <MovieName style={{fontFamily: 'GilroyHeavy'}}>{item?.name}</MovieName>
                                    </View>
                                </SliderItem>
                            </TouchableOpacity>
                        ))}
                    </Slider>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end', paddingLeft: 18, paddingRight: 18 }}>
                        <Text style={{ color: '#f2264b', fontFamily: 'GilroyBold', fontSize: 18 }}>Anti-heróis</Text>
                        <Text style={{ color: '#b7b7c8', fontWeight: 'bold' }}>Ver tudo</Text>
                    </View>
                    <Slider
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{ paddingHorizontal: 16 }}>
                        {application?.antiHeroes.map(item => (
                            <TouchableOpacity key={item.name} style={{ borderRadius: 10, overflow: 'hidden', marginRight: 10 }} onPress={() => handleNavigateToDetail(item)}>
                                <SliderItem key={item?.name} source={item?.imagePath}>
                                    <View>
                                        <PersonName style={{fontFamily: 'GilroyMedium'}}>{item?.alterEgo}</PersonName>
                                        <MovieName style={{fontFamily: 'GilroyHeavy'}}>{item?.name}</MovieName>
                                    </View>
                                </SliderItem>
                            </TouchableOpacity>
                        ))}
                    </Slider>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end', paddingLeft: 18, paddingRight: 18 }}>
                        <Text style={{ color: '#f2264b', fontFamily: 'GilroyBold', fontSize: 18 }}>Alienígenas</Text>
                        <Text style={{ color: '#b7b7c8', fontWeight: 'bold' }}>Ver tudo</Text>
                    </View>
                    <Slider
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{ paddingHorizontal: 16 }}>
                        {application?.aliens.map(item => (
                            <TouchableOpacity key={item.name} style={{ borderRadius: 10, overflow: 'hidden', marginRight: 10 }} onPress={() => handleNavigateToDetail(item)}>
                                <SliderItem key={item?.name} source={item?.imagePath}>
                                    <View>
                                        <PersonName style={{fontFamily: 'GilroyMedium'}}>{item?.alterEgo}</PersonName>
                                        <MovieName style={{fontFamily: 'GilroyHeavy'}}>{item?.name}</MovieName>
                                    </View>
                                </SliderItem>
                            </TouchableOpacity>
                        ))}
                    </Slider>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end', paddingLeft: 18, paddingRight: 18 }}>
                        <Text style={{ color: '#f2264b', fontFamily: 'GilroyBold', fontSize: 18 }}>Humanos</Text>
                        <Text style={{ color: '#b7b7c8', fontWeight: 'bold' }}>Ver tudo</Text>
                    </View>
                    <Slider
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{ paddingHorizontal: 16 }}>
                        {application?.humans.map(item => (
                            <TouchableOpacity key={item?.name} style={{ borderRadius: 10, overflow: 'hidden', marginRight: 10 }} onPress={() => handleNavigateToDetail(item)}>
                                <SliderItem key={item?.name} source={item?.imagePath}>
                                    <View>
                                        <PersonName style={{fontFamily: 'GilroyMedium'}}>{item?.alterEgo}</PersonName>
                                        <MovieName style={{fontFamily: 'GilroyHeavy'}}>{item?.name}</MovieName>
                                    </View>
                                </SliderItem>
                            </TouchableOpacity>
                        ))}
                    </Slider>
                </BoxPersonagens>
            </Box>
        </>
    )
}

export default Home;